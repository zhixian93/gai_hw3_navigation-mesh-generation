'''
 * Copyright (c) 2014, 2015 Entertainment Intelligence Lab, Georgia Institute of Technology.
 * Originally developed by Mark Riedl.
 * Last edited by Mark Riedl 05/2015
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
'''

import sys, pygame, math, numpy, random, time, copy, operator
from pygame.locals import *

from constants import *
from utils import *
from core import *

# Creates a path node network that connects the midpoints of each nav mesh together
def myCreatePathNetwork(world, agent = None):
    nodes = []
    edges = []
    polys = []
    ### YOUR CODE GOES BELOW HERE ###
    width = world.dimensions[0]
    height = world.dimensions[1]
    obstacles = world.obstacles
    candidatePoints = [(0,0),(width,0),(0,height),(width,height)]


    #part 1
    lines = []
    for obstacle in obstacles:
        candidatePoints += obstacle.points
        lines += obstacle.lines

    for i in range(len(candidatePoints)):
        point1 = candidatePoints[i]
        point2list = []
        for j in range(i + 1, len(candidatePoints)):
            point2 = candidatePoints[j]
            judge = True

            #remove the lines inside obstacles
            for obstacle in obstacles:
                tempPoints = obstacle.points
                tempLines = obstacle.lines
                if point1 in tempPoints and point2 in tempPoints and (point1, point2) not in tempLines and (point2, point1) not in tempLines:
                    arrow = numpy.array([point1[0] - point2[0], point1[1] - point2[1]])
                    norm = numpy.linalg.norm(arrow)
                    intersec = point1 + 1000.0 / norm * arrow
                    count = 0
                    for line in tempLines:
                        temp = calculateIntersectPoint(intersec, point1, line[0], line[1])
                        if temp == None:
                            continue
                        temp = (round(temp[0],1), round(temp[1], 1))
                        if temp not in tempPoints:
                            count += 1
                        else:
                            idx = tempPoints.index(temp)
                            templast = numpy.array(tempPoints[idx - 1])
                            tempnext = numpy.array(tempPoints[idx + 1 - len(tempPoints)])
                            temp = numpy.array(temp)
                            vec1 = templast - temp
                            vec2 = tempnext - temp
                            product1 = arrow[0] * vec1[1] - arrow[1] * vec1[0]
                            product2 = arrow[0] * vec2[1] - arrow[1] * vec2[0]
                            if product1 * product2 > 0:
                                pass
                            else:
                                count += 0.5
                    if count % 2  == 0:
                        continue

                if point1 in tempPoints and point2 in tempPoints and (point1, point2) not in tempLines and (point2, point1) not in tempLines:
                    judge = False
                    break

            #remove the lines conflicting with existing lines
            for line in lines:
                if (line[0] == point2 and line[1] == point1) or (line[0] == point1 and line[1] == point2):
                    continue
                temp = rayTrace(point1, point2, line)
                if temp != None:
                    temp = list(temp)
                    temp[0] = round(temp[0], 1)
                    temp[1] = round(temp[1], 1)
                    temp = tuple(temp)
                    if temp != point1 and temp != point2:
                        judge = False
                        break

            if judge:
                point2list.append(point2)


        for j in range(len(point2list)):
            point2 = point2list[j]

            for k in range(j+1, len(point2list)):
                point3 = point2list[k]
                judge = True

                for obstacle in obstacles:
                    tempPoints = obstacle.points
                    tempLines = obstacle.lines
                    if point3 in tempPoints and point2 in tempPoints and (point3, point2) not in tempLines and (
                    point2, point3) not in tempLines:
                        judge = False
                        break
                if not judge:
                    continue

                for point2can in point2list:
                    if point2can != point2 and point2can != point3 and calculateIntersectPoint(point1, point2can, point2, point3) != None:
                        judge = False
                if not judge:
                    continue

                for line in lines:
                    if (line[0] == point2 and line[1] == point3) or (line[0] == point3 and line[1] == point2):
                        continue
                    temp = rayTrace(point2, point3, line)
                    if temp != None:
                        temp = list(temp)
                        temp[0] = round(temp[0], 1)
                        temp[1] = round(temp[1], 1)
                        temp = tuple(temp)
                        if temp != point3 and temp != point2:
                            judge = False
                            break
                if not judge:
                    continue

                polygon = [point1, point2, point3]
                for point in candidatePoints:
                    if point != point1 and point != point2 and point != point3 and pointInsidePolygonPoints(point, polygon):
                        judge = False
                        break
                if judge:
                    polys.append(polygon)
                    judge12 = True
                    judge13 = True
                    judge23 = True
                    for line in lines:
                        if (line[0] == point1 and line[1] == point2) or (line[0] == point2 and line[1] == point1):
                            judge12 == False
                        if (line[0] == point2 and line[1] == point3) or (line[0] == point3 and line[1] == point2):
                            judge23 == False
                        if (line[0] == point1 and line[1] == point3) or (line[0] == point3 and line[1] == point1):
                            judge13 == False
                    if judge12:
                        lines.append((point1, point2))
                    if judge13:
                        lines.append((point1, point3))
                    if judge23:
                        lines.append((point2, point3))

    polys2 = polys[:]
    polys = []
    for i in range(len(polys2)):
        poly1 = polys2[i]
        change = False
        for j in range(i + 1, len(polys2)):
            poly2 = polys2[j]
            if not polygonsAdjacent(poly1, poly2):
                continue
            polynew = []
            comPoints = commonPoints(poly1, poly2)

            part1 = []
            for k in range(-len(poly1),0):
                if poly1[k] != comPoints[0] and poly1[k] != comPoints[1]:
                    pass
                elif poly1[k + 1] != comPoints[0] and poly1[k + 1] != comPoints[1]:
                    break
            for m in range(k + 1, len(poly1) + k - 1):
                part1.append(poly1[m])
            if poly1[k] == comPoints[0]:
                part1.reverse()

            part2 = []
            for k in range(-len(poly2), 0):
                if poly2[k] != comPoints[0] and poly2[k] != comPoints[1]:
                    pass
                elif poly2[k + 1] != comPoints[0] and poly2[k + 1] != comPoints[1]:
                    break
            for m in range(k + 1, len(poly2) + k - 1):
                part2.append(poly2[m])
            if poly2[k] == comPoints[1]:
                part2.reverse()

            polynew = part1 + [comPoints[0]] + part2 + [comPoints[1]]

            if isConvex(polynew):
                polys2[j] = polynew
                change = True
                break
        if change:
            continue
        polys.append(poly1)


    #part 2
    pointset = []
    for poly in polys:
        midpoint = [0, 0]
        for i in range(len(poly)):
            midpoint[0] = round((poly[i][0] + poly[i - 1][0])/2.0, 1)
            midpoint[1] = round((poly[i][1] + poly[i - 1][1])/2.0, 1)
            pointset.append(tuple(midpoint))

    radius = agent.getMaxRadius()
    for point in pointset:
        judge = True
        for obstacle in obstacles:
            for line in obstacle.lines:
                if minimumDistance(line, point) < radius:
                    judge = False
                    break
            if not judge:
                break
        if judge:
            nodes.append(point)

    #part 3
    edges = []
    for i in range(len(nodes)):
        for j in range(i + 1, len(nodes)):
            pathpoint1 = nodes[i]
            pathpoint2 = nodes[j]
            vec = numpy.array([pathpoint2[0] - pathpoint1[0], pathpoint2[1] - pathpoint1[1]])
            vecdis = numpy.linalg.norm(vec)
            vec = vec * radius / vecdis
            vec[0] = (math.ceil(vec[0]) if vec[0] > 0 else math.floor(vec[0]))
            vec[1] = (math.ceil(vec[1]) if vec[1] > 0 else math.floor(vec[1]))

            nor = numpy.array([pathpoint1[1] - pathpoint2[1], pathpoint2[0] - pathpoint1[0]])
            nordis = numpy.linalg.norm(nor)
            nor = nor * 1.1 * radius / nordis
            nor[0] = (math.ceil(nor[0]) if nor[0] > 0 else math.floor(nor[0]))
            nor[1] = (math.ceil(nor[1]) if nor[1] > 0 else math.floor(nor[1]))

            recpoint1 = (pathpoint1[0] - vec[0] - nor[0], pathpoint1[1] - vec[1] - nor[1])
            recpoint2 = (pathpoint1[0] - vec[0] + nor[0], pathpoint1[1] - vec[1] + nor[1])
            recpoint3 = (pathpoint2[0] + vec[0] - nor[0], pathpoint2[1] + vec[1] - nor[1])
            recpoint4 = (pathpoint2[0] + vec[0] + nor[0], pathpoint2[1] + vec[1] + nor[1])

            flag = False
            for obstacle in obstacles:
                for line in obstacle.getLines():
                    if calculateIntersectPoint(recpoint1, recpoint2, line[0], line[1]) != None:
                        flag = True
                        break
                    if calculateIntersectPoint(recpoint1, recpoint3, line[0], line[1]) != None:
                        flag = True
                        break
                    if calculateIntersectPoint(recpoint2, recpoint4, line[0], line[1]) != None:
                        flag = True
                        break
                    if calculateIntersectPoint(recpoint3, recpoint4, line[0], line[1]) != None:
                        flag = True
                        break
                if flag:
                    break
            if not flag:
                edges.append([pathpoint1, pathpoint2])
    ### YOUR CODE GOES ABOVE HERE ###
    return nodes, edges, polys


